package operador.aritmetica;

import operando.OperandoNumerico;
import org.junit.Assert;
import org.junit.Test;

public class DivisionTest {
    
    @Test
    public void testMultiplica2Enteros(){
        Division division = new Division();
        OperandoNumerico a = new OperandoNumerico(3);
        OperandoNumerico b = new OperandoNumerico(3);
        OperandoNumerico c = (OperandoNumerico)division.operar(a, b);
        
        OperandoNumerico d = new OperandoNumerico(1);
        
        Assert.assertEquals(d, c);
    }
}
