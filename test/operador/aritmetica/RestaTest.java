package operador.aritmetica;

import operando.OperandoNumerico;
import org.junit.Assert;
import org.junit.Test;

public class RestaTest {
    @Test
    public void testResta2Enteros(){
        Resta resta = new Resta();
        OperandoNumerico a = new OperandoNumerico(12);
        OperandoNumerico b = new OperandoNumerico(8);
        OperandoNumerico c = (OperandoNumerico)resta.operar(a, b);
        
        OperandoNumerico d = new OperandoNumerico(4);
        
        Assert.assertEquals(d, c);
    }
}
