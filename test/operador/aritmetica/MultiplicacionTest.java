package operador.aritmetica;

import operando.OperandoNumerico;
import org.junit.Assert;
import org.junit.Test;

public class MultiplicacionTest {
    
    @Test
    public void testMultiplica2Enteros(){
        Multiplicacion m = new Multiplicacion();
        OperandoNumerico a = new OperandoNumerico(5);
        OperandoNumerico b = new OperandoNumerico(8);
        OperandoNumerico c = (OperandoNumerico)m.operar(a, b);
        
        OperandoNumerico d = new OperandoNumerico(40);
        
        Assert.assertEquals(d, c);
    }
}
