package operador.aritmetica;

import operando.IOperandoNumerico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class ResiduoTest {

    @Test
    public void testOperar() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(17);
        IOperandoNumerico b = new OperandoNumerico(5);
        Residuo instance = new Residuo();
        IOperandoNumerico expResult = new OperandoNumerico(2);
        IOperandoNumerico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
    
}
