package operador.aritmetica;

import operando.OperandoNumerico;
import org.junit.Assert;
import org.junit.Test;

public class SumaTest {
    
    @Test
    public void testSuma2Enteros(){
        Suma suma = new Suma();
        OperandoNumerico operandoA = new OperandoNumerico(5);
        OperandoNumerico operandoB = new OperandoNumerico(8);
        OperandoNumerico result = (OperandoNumerico)suma.operar(operandoA, operandoB);
        
        OperandoNumerico expectedResult = new OperandoNumerico(13);
        
        Assert.assertEquals(expectedResult, result);
    }
}
