package operador.logica;

import operando.OperandoLogico;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class AndTest {
    
    @Test
    public void testAndTT() {
        And and = new And();
        OperandoLogico a = new OperandoLogico("T");
        OperandoLogico b = new OperandoLogico("T");
        OperandoLogico c = new OperandoLogico("T");
        
        OperandoLogico d = and.operar(a, b);
        
        assertEquals(d, c);
    }
    
    @Test
    public void testAndTF() {
        And and = new And();
        OperandoLogico a = new OperandoLogico("T");
        OperandoLogico b = new OperandoLogico("F");
        OperandoLogico c = new OperandoLogico("F");
        
        OperandoLogico d = and.operar(a, b);
        
        assertEquals(d, c);
    }
    
    @Test
    public void testAndFT() {
        And and = new And();
        OperandoLogico a = new OperandoLogico("F");
        OperandoLogico b = new OperandoLogico("T");
        OperandoLogico c = new OperandoLogico("F");
        
        OperandoLogico d = and.operar(a, b);
        
        assertEquals(d, c);
    }
    
    @Test
    public void testAndFF() {
        And and = new And();
        OperandoLogico a = new OperandoLogico("F");
        OperandoLogico b = new OperandoLogico("F");
        OperandoLogico c = new OperandoLogico("F");
        
        OperandoLogico d = and.operar(a, b);
        
        assertEquals(d, c);
    }
}
