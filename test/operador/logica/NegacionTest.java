package operador.logica;

import operando.OperandoLogico;
import org.junit.Assert;
import org.junit.Test;

public class NegacionTest {
    
    @Test
    public void testNegacion_T() {
        Negacion negacion = new Negacion();
        OperandoLogico a = new OperandoLogico("T");
        OperandoLogico b = (OperandoLogico) negacion.operar(a);
        OperandoLogico c = new OperandoLogico("F");
        
        Assert.assertEquals(c, b);
    }
    
    @Test
    public void testNegacion_F() {
        Negacion negacion = new Negacion();
        OperandoLogico a = new OperandoLogico("F");
        OperandoLogico b = (OperandoLogico) negacion.operar(a);
        OperandoLogico c = new OperandoLogico("T");
        
        Assert.assertEquals(c, b);
    }
}
