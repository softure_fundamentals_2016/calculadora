package operador.logica;

import operando.OperandoLogico;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class OrTest {
    
    @Test
    public void testOrTT() {
        Or or = new Or();
        OperandoLogico t1 = new OperandoLogico("T");
        OperandoLogico t2 = new OperandoLogico("T");
        OperandoLogico t3 = or.operar(t1, t2);
        OperandoLogico t4 = new OperandoLogico("T");
        
        assertEquals(t4, t3);
    }
    
    @Test
    public void testOrTF() {
        Or or = new Or();
        OperandoLogico t1 = new OperandoLogico("T");
        OperandoLogico t2 = new OperandoLogico("F");
        OperandoLogico t3 = or.operar(t1, t2);
        OperandoLogico t4 = new OperandoLogico("T");
        
        assertEquals(t4, t3);
    }
    
    @Test
    public void testOrFT() {
        Or or = new Or();
        OperandoLogico t1 = new OperandoLogico("F");
        OperandoLogico t2 = new OperandoLogico("T");
        OperandoLogico t3 = or.operar(t1, t2);
        OperandoLogico t4 = new OperandoLogico("T");
        
        assertEquals(t4, t3);
    }
    
    @Test
    public void testOrFF() {
        Or or = new Or();
        OperandoLogico t1 = new OperandoLogico("F");
        OperandoLogico t2 = new OperandoLogico("F");
        OperandoLogico t3 = or.operar(t1, t2);
        OperandoLogico t4 = new OperandoLogico("F");
        
        assertEquals(t4, t3);
    }
}
