package operador.relacional;

import operando.IOperandoNumerico;
import operando.OperandoLogico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class MenorQueTest {

    @Test
    public void testOperarAMenorQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(7);
        IOperandoNumerico b = new OperandoNumerico(15);
        MenorQue instance = new MenorQue();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOperarAMayorQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(17);
        IOperandoNumerico b = new OperandoNumerico(15);
        MenorQue instance = new MenorQue();
        OperandoLogico expResult = new OperandoLogico("F");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
}
