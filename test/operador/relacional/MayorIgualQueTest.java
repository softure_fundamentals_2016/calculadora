package operador.relacional;

import operando.IOperandoNumerico;
import operando.OperandoLogico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class MayorIgualQueTest {

    @Test
    public void testOperarAMayorQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(500);
        IOperandoNumerico b = new OperandoNumerico(7);
        MayorIgualQue instance = new MayorIgualQue();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOperarAIgualQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(500);
        IOperandoNumerico b = new OperandoNumerico(500);
        MayorIgualQue instance = new MayorIgualQue();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOperarAMenorQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(56);
        IOperandoNumerico b = new OperandoNumerico(500);
        MayorIgualQue instance = new MayorIgualQue();
        OperandoLogico expResult = new OperandoLogico("F");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
}
