package operador.relacional;

import operando.IOperandoNumerico;
import operando.OperandoLogico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class IgualdadTest {
    
    @Test
    public void testOperar() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(100);
        IOperandoNumerico b = new OperandoNumerico(100);
        Igualdad instance = new Igualdad();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
}
