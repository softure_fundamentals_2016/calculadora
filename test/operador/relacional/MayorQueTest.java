package operador.relacional;

import operando.IOperandoNumerico;
import operando.OperandoLogico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class MayorQueTest {

    @Test
    public void testOperarAMayorQueB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(70);
        IOperandoNumerico b = new OperandoNumerico(60);
        MayorQue instance = new MayorQue();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testOperarBMayorQueA() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(50);
        IOperandoNumerico b = new OperandoNumerico(60);
        MayorQue instance = new MayorQue();
        OperandoLogico expResult = new OperandoLogico("F");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }    
}
