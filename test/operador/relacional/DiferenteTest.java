package operador.relacional;

import operando.IOperandoNumerico;
import operando.OperandoLogico;
import operando.OperandoNumerico;
import org.junit.Test;
import static org.junit.Assert.*;

public class DiferenteTest {
    
    @Test
    public void testOperarADiferenteB() {
        System.out.println("operar");
        IOperandoNumerico a = new OperandoNumerico(5);
        IOperandoNumerico b = new OperandoNumerico(9);
        Diferente instance = new Diferente();
        OperandoLogico expResult = new OperandoLogico("T");
        OperandoLogico result = instance.operar(a, b);
        assertEquals(expResult, result);
    }
}
