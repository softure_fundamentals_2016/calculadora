package aplicacion;

import operacion.Aritmetica;
import operacion.Logica;
import operacion.Relacional;
import operador.aritmetica.Suma;
import operador.logica.And;
import operador.relacional.MayorQue;
import operando.OperandoLogico;
import operando.OperandoNumerico;

public class Aplicacion {

    public static void main(String args[]) {

        // Operaciones
        Aritmetica aritmetica = new Aritmetica();
        Logica logica = new Logica();
        Relacional relacional = new Relacional();

        // Operadores
        Suma suma = new Suma();
        And and = new And();
        MayorQue mayorQue = new MayorQue();
        
        // Operandos
        OperandoNumerico cien = new OperandoNumerico(100);
        OperandoNumerico treinta = new OperandoNumerico(30);
        OperandoLogico t = new OperandoLogico("T");
        OperandoLogico f = new OperandoLogico("F");
        
        // Operaciones Aritmética
        OperandoNumerico resultadoNumerico = (OperandoNumerico) aritmetica.evaluar(suma, cien, treinta);

        System.out.println(resultadoNumerico.getValue()); // 130
        
        // Operaciones Lógicas       
        OperandoLogico resultadoLogico = (OperandoLogico) logica.evaluar(and, t, f);
        
        System.out.println(resultadoLogico.getValue()); // F
        
        // Operaciones Relacionales
        OperandoLogico resultadoRelacional = (OperandoLogico) relacional.evaluar(mayorQue, cien, treinta);

        System.out.println(resultadoRelacional.getValue()); // F
    }
}
