package operacion;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;

public interface IOperacionAritmetica {
    IOperandoNumerico evaluar(IOperadorBinarioNumerico operador, IOperandoNumerico a, IOperandoNumerico b);
}
