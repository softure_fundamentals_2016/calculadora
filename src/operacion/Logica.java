package operacion;

import operador.IOperadorBinarioLogico;
import operador.IOperadorUnarioLogico;
import operando.OperandoLogico;

public class Logica implements IOperacionLogica {

    @Override
    public OperandoLogico evaluar(IOperadorBinarioLogico operador, OperandoLogico a, OperandoLogico b) {
        return operador.operar(a, b);
    }

    @Override
    public OperandoLogico evaluar(IOperadorUnarioLogico operador, OperandoLogico a) {
        return operador.operar(a);
    }
}
