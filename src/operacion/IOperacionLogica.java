package operacion;

import operador.IOperadorBinarioLogico;
import operador.IOperadorUnarioLogico;
import operando.OperandoLogico;

public interface IOperacionLogica {
    OperandoLogico evaluar(IOperadorBinarioLogico operador, OperandoLogico a, OperandoLogico b);
    OperandoLogico evaluar(IOperadorUnarioLogico operador, OperandoLogico a);
}
