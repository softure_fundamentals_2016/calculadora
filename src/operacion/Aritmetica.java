package operacion;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;

public class Aritmetica implements IOperacionAritmetica {

    @Override
    public IOperandoNumerico evaluar(IOperadorBinarioNumerico operador, IOperandoNumerico a, IOperandoNumerico b) {
        return operador.operar(a, b);
    }
}
