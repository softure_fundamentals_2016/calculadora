package operacion;

import operador.IOperadorRelacional;
import operando.IOperandoNumerico;
import operando.OperandoLogico;

public class Relacional implements IOperacionRelacional {

    @Override
    public OperandoLogico evaluar(IOperadorRelacional operador, IOperandoNumerico a, IOperandoNumerico b) {
        return operador.operar(a, b);
    }
}
