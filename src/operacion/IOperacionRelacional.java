package operacion;

import operador.IOperadorRelacional;
import operando.IOperandoNumerico;
import operando.OperandoLogico;

public interface IOperacionRelacional {
    OperandoLogico evaluar(IOperadorRelacional operador, IOperandoNumerico a, IOperandoNumerico b);
}
