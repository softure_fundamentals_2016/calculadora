package operando;

public interface IOperandoNumerico {
    Number getValue();
    void setValue(Number obj);
}
