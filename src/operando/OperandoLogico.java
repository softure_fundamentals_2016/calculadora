package operando;

public class OperandoLogico implements IOperandoLiteral {

    private String valor;
    
    public OperandoLogico(String valor) {
        this.valor = valor;
    }
    
    @Override
    public String getValue() {
        return valor;
    }

    @Override
    public void setValue(String obj) {
        this.valor = obj;
    }  

    @Override
    public int hashCode() {
        return this.valor.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        OperandoLogico other = (OperandoLogico) obj;
        
        return this.getValue().equals(other.getValue());
    }
    
    @Override
    public String toString() {
        return this.getValue();
    }
}
