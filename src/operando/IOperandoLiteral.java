package operando;

public interface IOperandoLiteral {
    String getValue();
    void setValue(String obj);
}
