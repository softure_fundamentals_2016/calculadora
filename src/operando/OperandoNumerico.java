package operando;

public class OperandoNumerico implements IOperandoNumerico {

    private Number valor;

    public OperandoNumerico(Number valor) {
        this.valor = valor;
    }

    @Override
    public Number getValue() {
        return valor;
    }

    @Override
    public void setValue(Number obj) {
        this.valor = obj;
    }

    @Override
    public int hashCode() {
        return this.valor.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        OperandoNumerico other = (OperandoNumerico) obj;
        if (this.getValue().longValue() != other.getValue().longValue()) {
            return false;
        }

        return true;
    }
    
    @Override
    public String toString() {
        return this.getValue().toString();
    }
}
