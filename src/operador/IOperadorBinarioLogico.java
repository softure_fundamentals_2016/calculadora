package operador;

import operando.OperandoLogico;

public interface IOperadorBinarioLogico {
    OperandoLogico operar(OperandoLogico a, OperandoLogico b);
}
