package operador.aritmetica;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;
import operando.OperandoNumerico;

public class Residuo implements IOperadorBinarioNumerico {

    @Override
    public IOperandoNumerico operar(IOperandoNumerico a, IOperandoNumerico b) {
        
        long residuo = a.getValue().longValue();
        
        while ( b.getValue().longValue() <= residuo ) {
            residuo = residuo - b.getValue().longValue();
        }
        
        return new OperandoNumerico(residuo);
    }
}
