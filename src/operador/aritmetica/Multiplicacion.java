package operador.aritmetica;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;
import operando.OperandoNumerico;

public class Multiplicacion implements IOperadorBinarioNumerico {

    @Override
    public IOperandoNumerico operar(IOperandoNumerico a, IOperandoNumerico b) {
        
        long resultado = 0;

        for ( long i = 0L; i < a.getValue().longValue(); i++ ) {
            resultado = resultado + b.getValue().longValue();
        }
        
        return new OperandoNumerico(resultado);
    }
}
