package operador.aritmetica;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;
import operando.OperandoNumerico;

public class Division implements IOperadorBinarioNumerico {

    @Override
    public IOperandoNumerico operar(IOperandoNumerico a, IOperandoNumerico b) {
        
        long residuo = a.getValue().longValue();
        long contador = 0;
        
        while ( b.getValue().longValue() <= residuo ) {
            residuo = residuo - b.getValue().longValue();
            contador++;
        }
        
        return new OperandoNumerico(contador);
    }
}
