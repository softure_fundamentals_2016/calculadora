package operador.aritmetica;

import operador.IOperadorBinarioNumerico;
import operando.IOperandoNumerico;
import operando.OperandoNumerico;

public class Suma implements IOperadorBinarioNumerico {

    @Override
    public IOperandoNumerico operar(IOperandoNumerico a, IOperandoNumerico b) {
        return new OperandoNumerico(a.getValue().longValue() + b.getValue().longValue());
    }
}
