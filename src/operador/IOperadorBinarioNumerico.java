package operador;

import operando.IOperandoNumerico;

public interface IOperadorBinarioNumerico {
    IOperandoNumerico operar(IOperandoNumerico a, IOperandoNumerico b);
}
