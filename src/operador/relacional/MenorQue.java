package operador.relacional;

import operador.IOperadorRelacional;
import operando.IOperandoNumerico;
import operando.OperandoLogico;

public class MenorQue implements IOperadorRelacional {

    @Override
    public OperandoLogico operar(IOperandoNumerico a, IOperandoNumerico b) {
        boolean resultado;
        resultado = a.getValue().longValue() < b.getValue().longValue();
        
        String resultadoLogico = (resultado)?"T":"F";
        
        return new OperandoLogico(resultadoLogico);
    }
}
