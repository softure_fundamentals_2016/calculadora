package operador;

import operando.OperandoLogico;

public interface IOperadorUnarioLogico {
    OperandoLogico operar(OperandoLogico a);
}
