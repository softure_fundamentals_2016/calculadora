package operador;

import operando.IOperandoNumerico;
import operando.OperandoLogico;

public interface IOperadorRelacional {
    OperandoLogico operar(IOperandoNumerico a, IOperandoNumerico b);
}
