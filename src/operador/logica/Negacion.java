package operador.logica;

import operador.IOperadorUnarioLogico;
import operando.OperandoLogico;

public class Negacion implements IOperadorUnarioLogico {

    @Override
    public OperandoLogico operar(OperandoLogico a) {
        OperandoLogico resultado = null;
        
        if ( a.getValue().equals("F") ) {
            resultado = new OperandoLogico("T");
        } else if ( a.getValue().equals("T") ) {
            resultado = new OperandoLogico("F");
        }
        
        return resultado;
    }
}
