package operador.logica;

import operador.IOperadorBinarioLogico;
import operando.OperandoLogico;

public class And implements IOperadorBinarioLogico {

    @Override
    public OperandoLogico operar(OperandoLogico a, OperandoLogico b) {
        String operandoA = a.getValue();
        String operandoB = b.getValue();
        OperandoLogico resultado = new OperandoLogico("F");
        
        if ( operandoA.equals("T") && operandoB.equals("T") ) {
            resultado =  new OperandoLogico("T");
        }

        return resultado;
    }    
}
