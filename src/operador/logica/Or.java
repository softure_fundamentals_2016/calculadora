package operador.logica;

import operador.IOperadorBinarioLogico;
import operando.IOperandoLiteral;
import operando.OperandoLogico;

public class Or implements IOperadorBinarioLogico {

    @Override
    public OperandoLogico operar(OperandoLogico a, OperandoLogico b) {
        String operandoA = a.getValue();
        String operandoB = b.getValue();
        IOperandoLiteral operandoC = new OperandoLogico("T");
        
        if ( operandoA.equals("F") && operandoB.equals("F") ) {
            operandoC = new OperandoLogico("F");
        }   
                
        return (OperandoLogico) operandoC;
    }
}
